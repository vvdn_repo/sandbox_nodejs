var WebSocketServer = require('websocket').server;
var http = require('http');
var common = require('./common.js');
var command;
var count_client_app = 0;
var dev_id;

var server = http.createServer(function (request, response) {
    var body = "";
    console.log((new Date()) + ' Received request for ' + request);
    request.on('data', function (chunk) {
        body += chunk;
        sendDevicedata(body,response);
    });

    request.on("close", function(err) {
        console.log("request closed...");
    });
    response.writeHead(404);
})
server.listen(8000, function () {
    console.log((new Date()) + ' Server is listening on port 8000');
    port_mac = [];
});
wsServer = new WebSocketServer({
    httpServer: server,
    // You should not use autoAcceptConnections for production 
    // applications, as it defeats all standard cross-origin protection 
    // facilities built into the protocol and the browser.  You should 
    // *always* verify the connection's origin and decide whether or not 
    // to accept it. 
    autoAcceptConnections: false
});
function originIsAllowed(origin) {
    // put logic here to detect whether the specified origin is allowed. 
    return true;
}

process.on('uncaughtException', function (err) {
    console.log( "UNCAUGHT EXCEPTION " );
    console.log( "[Inside 'uncaughtException' event] " + err.stack || err.message );
});


wsServer.on('request', function (request) {
    if (!originIsAllowed(request.origin)) {
        // Make sure we only accept requests from an allowed origin 
        request.reject();
        console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.');
        return;
    }
    var connection = request.accept();
    var str = request.resourceURL.path;
    var parameter = str.replace("/", ""); //replace the / str in path
    // console.log('parameter' + parameter);
    //
//ws://127.0.0.1:8000/ device_mac=44:43:65:23:14&user_token=sb_100_a3ntf
    if (parameter != "" || parameter.length != 0 || parameter == null) {
        // console.log('parameter-------------------------')
        if (parameter.search("device_mac") !=-1 ) {
            //     console.log('in Device');
            var deviceMac = parameter.substring(parameter.lastIndexOf("device_mac=") + 11,
                parameter.lastIndexOf("&"));
            var userTocken = parameter.substring(parameter.lastIndexOf("=") + 1,
                parameter.lastIndexOf(""));
            console.log('deviceMac-----' + deviceMac);
            console.log('UserTocken---------' + userTocken);
            message = {"command": "device/get-device-identity", "device_mac": deviceMac, "user_token": userTocken};
            command = message.command;
            message = JSON.stringify(message);
        }
        common.callApi(message, command, http, connection);
    }
    console.log((new Date()) + ' Connection accepted.');
    console.log((new Date()) + ' Connection from origin ' + request.origin + ' started.');
    console.log(' Connection from' + deviceMac);
    console.log(connection.socket['_peername']['port']);
    port_mac.push({
        port: connection.socket['_peername']['port'],
        mac: deviceMac
    });
    message = {"commandId": "90", "command": "raise-alarm","device_mac":deviceMac,"reason": "device-connected"};
    command = message.command;
    message = JSON.stringify(message);
    common.callApi(message, command, http, connection);
    console.log('online called');
    connection.on('message', function (message) {
        if (message.type === 'utf8') {
            console.log('Received Message: ' + message.utf8Data);
            var parsed = JSON.parse(message.utf8Data);
            command = parsed.command;
            dev_id = parsed.device_id;
            common.callApi(message.utf8Data, command, http,connection,dev_id);
        }
    });
    connection.on('close', function (reasonCode, description) {
        console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
        console.log(connection.socket['_peername']['port']);
        for (var i = 0; i < port_mac.length; i++) {

            if (port_mac[i]['port'] == connection.socket['_peername']['port']) {
                //console.log(port_mac[i]['mac']);
                message = {"commandId": "90", "command": "raise-alarm","device_mac":port_mac[i]['mac'],"reason": "device-disconnected"};
                command = message.command;
                message = JSON.stringify(message);
                common.callApi(message, command, http, connection);

            }
        }
    });
});
process.on('uncaughtException', function (err) {
    console.error((new Date).toUTCString() + ' uncaughtException:', err.message)
    console.error('Exception occured'+err.stack)

});
function sendDevicedata(data,response)
{
    // console.log("data"+data);
    //var parsed =JSON.parse(data);
    // var source=parsed.commandId;
    //command=parsed.command;
    //if(source)
    //{
    common.callDevice(data,response);
    //}
    //else
    // {
//common.callApp(data);
//common.httpRes(data,response,command,http);
    // }
    //common.callApi(data, command, http);
}
